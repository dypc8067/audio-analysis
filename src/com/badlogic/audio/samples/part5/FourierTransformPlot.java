package com.badlogic.audio.samples.part5;

import java.awt.Color;
import java.util.logging.Logger;

import com.badlogic.audio.analysis.FFT;
import com.badlogic.audio.visualization.Plot;

/**
 * Simple example that generates a 1024 samples sine wave at 440Hz
 * and plots the resulting spectrum. 
 * 
 * @author mzechner
 *
 */
public class FourierTransformPlot 
{
	public static void main( String[] argv )
	{
		final float frequency = 440; // Note A		
		float increment = (float)(2*Math.PI) * frequency / 44100;	
		System.out.println("increment = " + increment);
		float angle = 0;		
		float samples[] = new float[1024];
		FFT fft = new FFT( 1024, 44100 );
		
		for( int i = 0; i < samples.length; i++ )
		{
			samples[i] = (float)Math.sin( angle );
			angle += increment;		
		}
		
		fft.forward( samples );
		
//		Plot plotSamples = new Plot( "Samples", 512, 512 );
//	    plotSamples.plot( samples, 2, Color.white );
	      
		Plot plot = new Plot( "Note A Spectrum", 1024, 512);
		plot.plot(samples, 1, Color.white);
		plot.plot(fft.getSpectrum(), 1, Color.red );
		
	}
}
